require 'httparty'

class Forecast
  include HTTParty
  format :json
  base_uri 'api.openweathermap.org'

  def initialize(attrs)
    geolocation = ::Geocoder.search(attrs[:location]).first
    @options = { 
      query: { 
        APPID: ENV['WEATHER_API_KEY'], 
        lat: geolocation.latitude, 
        lon: geolocation.longitude,
        units: 'metric' 
        } 
      }
  end

  def current
    self.class.get("/data/2.5/weather", @options)
  end

  def weekly
    self.class.get("/data/2.5/forecast", @options)
  end
end