require 'forecast'

class ForecastsController < ApplicationController
  def index
  end

  def show
    @forecasts = Forecast.new(params[:forecast])
  end
end
